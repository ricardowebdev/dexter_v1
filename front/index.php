    <header class="header">
        <?php include 'template/topo.php'; ?>
    </header>
    <?php
        $prepare = $pdo->prepare("SELECT * FROM banners");
        $prepare->execute();
        $banners = $prepare->fetchAll(PDO::FETCH_ASSOC);                
    ?>
    <section class="banner">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">                
                <?php foreach($banners as $banner): ?>
                    <?php $class = $banner['id'] == 2 ? 'item active' : 'item'; ?>
                    <div class="<?= $class; ?>" style="background-image:url('http://ricardowebdev.com.br/dexter/uploads/banners/<?= $banner['url']; ?>')">
                      <div class="container">
                        <div class="row">
                            <div class="span12">
                                <h2><?= $banner['nome']; ?></h2>
                                <p><?= $banner['descricao']; ?></p>
                            </div>
                        </div>
                      </div>
                   </div>
                <?php endforeach; ?>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </section>

    <section class="vantagens">
        <?php include 'template/vantagens.php'; ?>
    </section>

    <section class="funcionalidades">
        <?php include 'template/funcionalidades.php'; ?>
    </section>

    <section class="cadastro">
        <div class="container">
            <div class="left">
                <h3>Você cuida da sua empresa e nós da sua logistica.</h3>
                <p>Cadastre-se agora e tenha 20% de desconto no primeiro ano.</p>
            </div>
            <a href="cadastro.php" class="btn btn-flat right">Cadastre-se</a>
        </div>
    </section>

    <footer class="footer">
        <?php include 'template/rodape.php'; ?>
    </footer>
</body>

</html>
