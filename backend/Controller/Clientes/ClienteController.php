<?php
	namespace Controller\Clientes;

	use src\Conexao\Conexao as Conexao;
	use Model\Clientes\Cliente as Cliente;
	use src\Traits\MensagemTrait;
	use src\View\View;	

	class ClienteController
	{
		public static function listClientes($pdo)
		{
			$prepare = $pdo->prepare("SELECT * FROM clientes");
			$prepare->execute();
			$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);
			$clientes = [];

			foreach ($result as $dados) {
				$cliente = new Cliente();
				$cliente->setId($dados['id']);
				$cliente->setNomeRazao($dados['nome_razao']);
				$cliente->setEmail($dados['email']);
				$cliente->setTelefone($dados['telefone']);
				$cliente->setCelular($dados['celular']);
				$cliente->setCep($dados['cep']);
				$cliente->setEndereco($dados['endereco']);
				$cliente->setBairro($dados['bairro']);
				$cliente->setCidade($dados['cidade']);
				$cliente->setEstado($dados['estado']);

				$clientes[] = $cliente;
			}

			View::mountPage('Cliente', 'Listar', $clientes);
		}

		public static function updateCliente($pdo, $dados)
		{
			if(isset($_POST['id']) && !empty($_POST['id'])) {
				$sql = "UPDATE clientes SET nome_razao = :nome_razao,
						                    email = :email,
						                    telefone = :telefone,
						                    celular = :celular,
						                    cep = :cep,
						                    endereco = :endereco,
						                    bairro = :bairro,
						                    cidade = :cidade,
						                    estado = :estado
						WHERE id = :id";

				$prepare = $pdo->prepare($sql);
				$prepare->bindValue(":nome_razao", $dados['nome_razao']);
				$prepare->bindValue(":email",      $dados['email']);
				$prepare->bindValue(":telefone",   $dados['telefone']);
				$prepare->bindValue(":celular",    $dados['celular']);
				$prepare->bindValue(":cep",  	   $dados['cep']);
				$prepare->bindValue(":endereco",   $dados['endereco']);
				$prepare->bindValue(":bairro",     $dados['bairro']);
				$prepare->bindValue(":cidade",     $dados['cidade']);
				$prepare->bindValue(":estado",     $dados['estado']);
				$prepare->bindValue(":id", 		   $dados['id']);						

				if($prepare->execute()) {
					MensagemTrait::set("Cliente alterado com sucesso", "success");
				} else {
					MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
				}	

				View::redirect('listClientes');				
			} else {
				self::selectCliente($pdo, $_GET['id']);				
			}
		}

		public static function deleteCliente($pdo)
		{
            if(isset($_GET['id']) && !empty($_GET['id'])) {				
                $prepare = $pdo->prepare("DELETE FROM clientes WHERE id = ".$_GET['id']);

                if($prepare->execute()) 
                    MensagemTrait::set("Cliente removido com sucesso", "success");
                else 
                    MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
            }

            View::redirect('listClientes');
		}

		public static function insertCliente($pdo, $dados = "")
		{
			if(isset($dados['nome_razao']) && !empty($dados['nome_razao'])) {
				$sql = "INSERT INTO clientes (nome_razao,
											 email,
											 telefone,
											 celular,
											 cep,
											 endereco,
											 bairro,
											 cidade,
											 estado)
									 VALUES (:nome_razao,
									 		 :email,
									 		 :telefone,
									 		 :celular,
									 		 :cep,
									 		 :endereco,
									 		 :bairro,
									 		 :cidade,
									 		 :estado)";


				$prepare = $pdo->prepare($sql);
				$prepare->bindValue(":nome_razao", $dados['nome_razao']);
				$prepare->bindValue(":email",      $dados['email']);
				$prepare->bindValue(":telefone",   $dados['telefone']);
				$prepare->bindValue(":celular",    $dados['celular']);
				$prepare->bindValue(":cep",  	   $dados['cep']);
				$prepare->bindValue(":endereco",   $dados['endereco']);
				$prepare->bindValue(":bairro",     $dados['bairro']);
				$prepare->bindValue(":cidade",     $dados['cidade']);
				$prepare->bindValue(":estado",     $dados['estado']);

				if($prepare->execute()) {
					MensagemTrait::set("Cliente inserido com sucesso", "success");
				} else {
					MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
				}				

				View::redirect('listClientes');							
			} else {
				View::mountPage('Cliente', 'Cadastrar');
			}									
		}

		public static function selectCliente($pdo, $id)
		{
			$prepare = $pdo->prepare("SELECT * FROM clientes WHERE id = ".$id);
			$prepare->execute();
			$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);
			$cliente;

			foreach ($result as $dados) {
				$cliente = new Cliente();
				$cliente->setId($dados['id']);
				$cliente->setNomeRazao($dados['nome_razao']);
				$cliente->setEmail($dados['email']);
				$cliente->setTelefone($dados['telefone']);
				$cliente->setCelular($dados['celular']);
				$cliente->setCep($dados['cep']);
				$cliente->setEndereco($dados['endereco']);
				$cliente->setBairro($dados['bairro']);
				$cliente->setCidade($dados['cidade']);
				$cliente->setEstado($dados['estado']);				
			}	

			View::mountPage('Cliente', 'Editar', $cliente);
		}
	}