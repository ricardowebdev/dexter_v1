<div class="title">
	<h1>Funcionalidades</h1>
	<h3>Uma plataforma moderna e flexivel para gerenciar a logistica do seu
		negócio</h3>
</div>
<ul class="container">
	<?php
        $prepare = $pdo->prepare("SELECT * FROM funcionalidades");
        $prepare->execute();
        $funcionalidades = $prepare->fetchAll(PDO::FETCH_ASSOC);		
	?>
	<?php foreach ($funcionalidades as $funcionalidade) : ?>
	<li>
		<figure>
			<img src="http://ricardowebdev.com.br/dexter/uploads/funcionalidades/<?= $funcionalidade['url_icone']; ?>" alt="Nome Imagem">
			<figcaption>
				<strong><?= $funcionalidade['nome']; ?></strong>
				<p><?= $funcionalidade['descricao']; ?></p>
			</figcaption>
		</figure>
	</li>
    <?php endforeach; ?>
</ul>
