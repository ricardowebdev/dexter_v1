<html>
	<head>
		<title>Sistema administrativo Dexter</title>
		<meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">		
		<link rel="stylesheet" type="text/css" href="src/assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="src/assets/css/style.css">
	</head>
	<body>	
		<div class="page-header">
		    <h1><b>Sistema administrativo Dexter</b></h1>
		</div>
		<?php 
			session_start();
			include "src/Traits/MensagemTrait.php";
			use src\Traits\MensagemTrait;
			MensagemTrait::get(); 
		?><br><br>		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4">&nbsp;</div>
				<div class="col-md-4 center">
					<h2>Login</h2>
				</div>
				<div class="col-md-4">&nbsp;</div>
			</div><br>
			<form method="POST" action="index.php">
				<div class="row">
					<div class="col-md-4">&nbsp;</div>
					<div class="col-md-4">
						<label>Usuario</label>
						<input type="text" name="nome" class="form-control" placeholder="Usuario" required>
					</div>
					<div class="col-md-4">&nbsp;</div>
				</div><br>

				<div class="row">
					<div class="col-md-4">&nbsp;</div>
					<div class="col-md-4">
						<label>Senha</label>
						<input type="password" name="senha" class="form-control" placeholder="Senha" required>
					</div>
					<div class="col-md-4">&nbsp;</div>
				</div><br>

				<div class="row">
					<div class="col-md-4 col-md-offset-4">					
						<button class="btn btn-primary form-control"><b>Login</b></button>
					</div>
				</div><br>
			</form>
		</div>
		<script type="text/javascript" src="src/assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="src/assets/js/bootstrap.min.js"></script>
	</body>
</html>