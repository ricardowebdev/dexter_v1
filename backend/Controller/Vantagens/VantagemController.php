<?php

	namespace Controller\Vantagens;

	use src\Conexao\Conexao as Conexao;
	use Model\Vantagens\Vantagem as Vantagem;
	use src\Traits\MensagemTrait;
	use src\View\View;	

	class VantagemController
	{

		public function __construct()
		{
			
		}

		public static function listVantagens($pdo)
		{
			$prepare = $pdo->prepare("SELECT * FROM vantagens");
			$prepare->execute();
			$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);
			$vantagens = [];

			foreach ($result as $dados) {
				$vantagem = new Vantagem();
				$vantagem->setId($dados['id']);
				$vantagem->setNome($dados['nome']);
				$vantagem->setDescricao($dados['descricao']);
				$vantagem->setUrlIcone($dados['url_icone']);

				$vantagens[] = $vantagem;
			}

			View::mountPage('Vantagem', 'Listar', $vantagens);			
		}

        public static function deleteVantagem($pdo)
		{
            if(isset($_GET['id']) && !empty($_GET['id'])) {				
                $prepare = $pdo->prepare("DELETE FROM vantagens WHERE id = ".$_GET['id']);

                if($prepare->execute()) 
                    MensagemTrait::set("Vantagem removida com sucesso", "success");
                else 
                    MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
            }

            View::redirect('listVantagens');										
		}

        public static function selectVantagem($pdo, $id)
		{
			$prepare = $pdo->prepare("SELECT * FROM vantagens WHERE id = ".$id);
			$prepare->execute();
			$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);

			foreach ($result as $dados) {				
				$folder = $dados['url_icone'];

				$vantagem = new Vantagem();
				$vantagem->setId($dados['id']);
				$vantagem->setNome($dados['nome']);
				$vantagem->setDescricao($dados['descricao']);
				$vantagem->setUrlIcone($folder);				
			}	

			View::mountPage('Vantagem', 'Editar', $vantagem);			
		}

        public static function updateVantagem($pdo, $dados)
		{
			if (isset($_POST['id']) && !empty($_POST['id'])) {
				
				$vantagem = new Vantagem;


				// Cuidando da atualização da foto
				if (isset($_FILES['url_icone']) && !empty($_FILES['url_icone'])) {
					if ($vantagem->removeVantagemOld($_POST['urlOld'])) {

						$url = $vantagem->uploadVantagem();

						if ($url['status'] == "error") {
							MensagemTrait::set($url['msg'], "danger");
							View::redirect('listVantagens');				
						} else {
							$dados['url_icone'] = $url['msg'];
						}

					} else {
						MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
						View::redirect('listVantagens');
					}
				} else {
					$dados['url_icone'] = $dados['urlOld'];
				}


				$sql = "UPDATE vantagens SET nome = :nome,
						                  descricao = :descricao,
						                  url_icone = :url_icone
						WHERE id = :id";

				$prepare = $pdo->prepare($sql);
				$prepare->bindValue(":nome",      $dados['nome']);
				$prepare->bindValue(":descricao", $dados['descricao']);
				$prepare->bindValue(":url_icone", $dados['url_icone']);
				$prepare->bindValue(":id", 		  $dados['id']);						

				if($prepare->execute()) {
					MensagemTrait::set("Vantagem alterada com sucesso", "success");
				} else {
					MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
				}	

				View::redirect('listVantagens');				
			} else {
				self::selectVantagem($pdo, $_GET['id']);				
			}
					
		}

        public static function insertVantagem($pdo, $dados)
		{
			if (isset($_POST['nome']) && !empty($_POST['nome'])) {
				$vantagem = new Vantagem;

				// Realizando o upload da foto
				$url = $vantagem->uploadVantagem();

				if ($url['status'] == "error") {
					MensagemTrait::set($validate, "danger");
					View::redirect('newVantagem');				
				} else {
					$dados['url_icone'] = $url['msg'];
				}

				//Validando os Dados			
				$validate = $vantagem->validaDados($dados);

				if ($validate != "true") {
					MensagemTrait::set($validate, "danger");
					View::redirect('newVantagem');
				} else {

					$sql = "INSERT INTO vantagens (nome,
												   descricao,
												   url_icone)
										   VALUES (:nome,
										 	  	   :descricao,
										 	  	   :url_icone)";

					$prepare = $pdo->prepare($sql);
					$prepare->bindValue(":nome",      $dados['nome']);
					$prepare->bindValue(":descricao", $dados['descricao']);
					$prepare->bindValue(":url_icone", $dados['url_icone']);

					if($prepare->execute()) {
						MensagemTrait::set("Vantagem inserida com sucesso", "success");
					} else {
						MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
					}				

					View::redirect('listVantagens');							
				}	

			} else {
				View::mountPage('Vantagem', 'Cadastrar');
			}
		}
	}