<?php
	namespace Controller\Funcionarios;

	use src\Conexao\Conexao as Conexao;
	use Model\Funcionarios\Funcionario as Funcionario;
	use src\Traits\MensagemTrait;
	use src\View\View;	

	class FuncionarioController
	{
		public static function listFuncionarios($pdo)
		{
			$prepare = $pdo->prepare("SELECT * FROM funcionarios");
			$prepare->execute();
			$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);
			$funcionarios = [];

			foreach ($result as $dados) {
				$funcionario = new Funcionario();
				$funcionario->setId($dados['id']);
				$funcionario->setIdPrf($dados['prf_id']);
				$funcionario->setNome($dados['nome']);
				$funcionario->setEmail($dados['email']);
				$funcionario->setSenha($dados['senha']);

				$funcionarios[] = $funcionario;
			}

			View::mountPage('Funcionario', 'Listar', $funcionarios);
		}

		public static function updateFuncionario($pdo, $dados)
		{
			if(isset($_POST['id']) && !empty($_POST['id'])) {
				if($dados['senha'] == $dados['senhaOld'])
					$senha = $dados['senha'];
				else 
					$senha = md5($dados['senha']);

				$sql = "UPDATE funcionarios SET prf_id = :prf_id,
						                    	   nome = :nome,
						                    	   email = :email,
						                    	   senha = :senha
						WHERE id = :id";

				$prepare = $pdo->prepare($sql);
				$prepare->bindValue(":prf_id", $dados['prf_id']);
				$prepare->bindValue(":nome",   $dados['nome']);
				$prepare->bindValue(":email",  $dados['email']);
				$prepare->bindValue(":senha",  $senha);
				$prepare->bindValue(":id", 	   $dados['id']);						

				if($prepare->execute()) {
					MensagemTrait::set("Funcionario alterado com sucesso", "success");
				} else {
					MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
				}	

				View::redirect('listFuncionarios');				
			} else {
				self::selectFuncionario($pdo, $_GET['id']);				
			}
		}

		public static function deleteFuncionario($pdo)
		{
            if(isset($_GET['id']) && !empty($_GET['id'])) {				
                $prepare = $pdo->prepare("DELETE FROM funcionarios WHERE id = ".$_GET['id']);

                if($prepare->execute()) 
                    MensagemTrait::set("Funcionario removido com sucesso", "success");
                else 
                    MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
            }

            View::redirect('listFuncionarios');
		}

		public static function insertFuncionario($pdo, $dados)
		{
			if(isset($dados['nome']) && !empty($dados['nome'])) {
				$senha = md5($dados['senha']);

				$sql = "INSERT INTO funcionarios (prf_id,
											      nome,
											      email,
											      senha)
									      VALUES (:prf_id,
									 	  	      :nome,
									 		      :email,
									 		      :senha)";

				$prepare = $pdo->prepare($sql);
				$prepare->bindValue(":prf_id", $dados['prf_id']);
				$prepare->bindValue(":nome",   $dados['nome']);
				$prepare->bindValue(":email",  $dados['email']);			
				$prepare->bindValue(":senha",  $senha);			

				if($prepare->execute()) {
					MensagemTrait::set("Funcionario inserido com sucesso", "success");
				} else {
					MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
				}				

				View::redirect('listFuncionarios');							
			} else {
				View::mountPage('Funcionario', 'Cadastrar');
			}	
		}

		public static function selectFuncionario($pdo, $id)
		{
			$prepare = $pdo->prepare("SELECT * FROM funcionarios WHERE id = ".$id);
			$prepare->execute();
			$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);
			$funcionario;

			foreach ($result as $dados) {
				$funcionario = new Funcionario();
				$funcionario->setId($dados['id']);
				$funcionario->setIdPrf($dados['prf_id']);
				$funcionario->setNome($dados['nome']);
				$funcionario->setEmail($dados['email']);
				$funcionario->setSenha($dados['senha']);
			}	

			View::mountPage('Funcionario', 'Editar', $funcionario);						
		}
	}