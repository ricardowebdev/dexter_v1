<!doctype html>
<html dir="ltr" lang="pt-BR">
<head>
	<meta charset="utf-8">
	<title>Dexter Courier | Cadastre-se</title>
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<header class="header">
        <?php include 'template/topo.php'; ?>
    </header>

    <?php if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])) : ?>
		<div class="alert alert-<?= $_SESSION['status']; ?>">
		  <?= $_SESSION['msg']; ?>
		</div>    	
    <?php unset($_SESSION['msg']); endif; ?>

	<div class="container content">
		<div class="title center">
			<h1>Cadastre-se</h1>
		</div>

		<div class="banner-fixed">
			<img src="img/banner-cadastro.jpg" alt="Banner Sobre">
		</div>

		<form action="cadastrar.php" method="POST" class="form">
			<div class="row">
				<div class="col-md-offset-3 col-md-3 text-center">
					<label for="nome">Nome/Razao</label> 
					<input type="text" name="nome_razao" id="nome_razao" required class="form-control" placeholder="Digite seu nome ou Razão Social">
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-3 col-md-3 text-center">
					<label for="email">Email</label> 
					<input type="email" name="email" id="email" required class="form-control" placeholder="Digite seu E-mail">
				</div>
			</div>

			<div class="row">
				<div class="col-md-offset-3 col-md-3 text-center">
					<label for="tel">Telefone</label> 
					<input type="tel" name="telefone" id="telefone" required placeholder="Digite seu telefone">
				</div>
			</div>						

			<div class="row">
				<div class="col-md-offset-3 col-md-3 text-center">
					<label for="cel">Celular</label> 
					<input type="tel" name="celular" id="celular" required placeholder="Digite seu celular">
				</div>
			</div>						

			<div class="row">
				<div class="col-md-offset-3 col-md-3 text-center">
					<label for="cep">Cep</label> 
					<input type="tel" name="cep" id="cep" required placeholder="Digite seu Cep">				
				</div>
			</div>

			<div class="row">
				<div class="col-md-offset-3 col-md-3 text-center">
					<label for="endereco">Endereço</label> 
					<input type="tel" name="endereco" id="endereco" required placeholder="Digite seu endereço">
				</div>
			</div>

			<div class="row">
				<div class="col-md-offset-3 col-md-3 text-center">
					<label for="bairro">Bairro</label> 
					<input type="tel" name="bairro" id="bairro" required placeholder="Digite seu Bairro">
				</div>
			</div>

			<div class="row">
				<div class="col-md-offset-3 col-md-3 text-center">
					<label for="cidade">Cidade</label> 
					<input type="tel" name="cidade" id="cidade" required placeholder="Digite sua Cidade">
				</div>
			</div>

			<div class="row">
				<div class="col-md-offset-3 col-md-3 text-center">
					<label for="Estado">Estado</label>
					<select name="estado" id="estado">
						<option value="">Selecione</option>
						<option value="AC">AC</option>
						<option value="AL">AL</option>
						<option value="AP">AP</option>
						<option value="AM">AM</option>
						<option value="BA">BA</option>
						<option value="CE">CE</option>
						<option value="DF">DF</option>
						<option value="ES">ES</option>
						<option value="GO">GO</option>
						<option value="MA">MA</option>
						<option value="MT">MT</option>
						<option value="MS">MS</option>
						<option value="MG">MG</option>
						<option value="PA">PA</option>
						<option value="PB">PB</option>
						<option value="PR">PR</option>
						<option value="PE">PE</option>
						<option value="PI">PI</option>
						<option value="RJ">RJ</option>
						<option value="RN">RN</option>
						<option value="RS">RS</option>
						<option value="RO">RO</option>
						<option value="RR">RR</option>
						<option value="SC">SC</option>
						<option value="SP">SP</option>
						<option value="SE">SE</option>
						<option value="TO">TO</option>
					</select>
				</div>
			</div>

			<div class="row">
				<div class="col-md-offset-3 col-md-3 text-center">
					<button class="btn">Enviar</button>
				</div>
			</div>				
		</form>
	</div>

	<footer class="footer">
        <?php include 'template/rodape.php'; ?>
    </footer>
</body>
</html>
