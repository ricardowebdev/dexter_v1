	<br><div class="container-fluid">
		<div class="row">
			<div class="col-md-3">&nbsp;</div>
			<div class="col-md-6 center"><h1><b>Clientes</b></h1></div>
			<div class="col-md-3">&nbsp;</div>
		</div><br>
		<div class="row">			
			<div class="col-md-2">
				<a href="index.php?route=newCliente" class="btn btn-primary">
					<b><i class="glyphicon glyphicon-plus"></i>&nbsp;Adicionar</b>
				</a>
			</div>
		</div>
		<div class="row">			
			<div class="col-md-12" style="padding: 25px;">
				<table class="table table-striped" id="table">
				    <thead>
				        <tr>
				            <th>#</th>
				            <th>Nome</th>
				            <th>E-mail</th>
				            <th>Telefone</th>
				            <th>Cidade</th>
				            <th>UF</th>
				            <th>Ações</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($dados as $cliente) : ?>
				    		<tr>	
					        	<th scope="row"><?= $cliente->getId(); ?></th>		         
					            <td><?= $cliente->getNomeRazao(); ?></td>
					            <td><?= $cliente->getEmail();     ?></td>
					            <td><?= $cliente->getTelefone();  ?></td>
					            <td><?= $cliente->getCidade();    ?></td>
					            <td><?= $cliente->getEstado();    ?></td>
					            <td>
					            	<a href="index.php?route=editCliente&id=<?= $cliente->getId(); ?>" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-pencil"></i></a>
					            	<a onclick="callDelete('Cliente', <?= $cliente->getId(); ?>)" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal"><i class="glyphicon glyphicon-trash"></i></a>
					            </td>
					        </tr>
					    <?php endforeach ?>    
	  			    </tbody>
				</table>				
			</div>
			<div class="col-md-1">&nbsp;</div>
		</div>
	</div>