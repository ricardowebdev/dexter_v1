<br><div class="container">
	<div class="row">
		<div class="col-md-3">&nbsp;</div>
		<div class="col-md-6 center"><h1><b>Novo Banner</b></h1></div>
		<div class="col-md-3">&nbsp;</div>
	</div><hr>

	<form method="POST" action="index.php?route=newBanner" enctype="multipart/form-data">	
		<input type="hidden" name="id">
		<div class="row">
			<div class="col-md-6">
				<label>Nome</label>
				<input type="text" name="nome" id="nome" placeholder="Nome" class="form-control" required>
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-6">
				<label>Url</label>
				<input type="file" name="url" id="url">
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-10">
				<label>Descrição</label>
				<textarea name="descricao" id="descricao" placeholder="Descricao" class="form-control" required></textarea>
			</div>
		</div><br>	

		<div class="row">
			<div class="col-md-2">
				<button class="btn btn-success form-control"><b><i class="glyphicon glyphicon-plus"></i>&nbsp;Cadastrar</b></button>
			</div>&nbsp;

			<div class="col-md-2">
				<a class="btn btn-danger form-control" href="index.php?route=listBanners"><b><i class="glyphicon glyphicon-remove"></i>&nbsp;Cancelar</b></a>
			</div>&nbsp;
		</div>
	</form>

</div>