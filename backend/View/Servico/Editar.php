<br><div class="container">
	<div class="row">
		<div class="col-md-3">&nbsp;</div>
		<div class="col-md-6 center"><h1><b>Edição dos Serviços</b></h1></div>
		<div class="col-md-3">&nbsp;</div>
	</div><hr>

	<form method="POST" action="index.php?route=editServico" enctype="multipart/form-data" enctype="multipart/form-data">	
		<input type="hidden" name="id" value="<?= $dados->getId(); ?>">
		<div class="row">
			<div class="col-md-6">
				<label>Perfil</label>
				<input type="text" name="nome" id="nome" placeholder="Nome" class="form-control" required value="<?= $dados->getNome(); ?>">
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-6">
				<label>Descrição</label>
				<input type="text" name="descricao" id="descricao" placeholder="Descrição" class="form-control" value="<?= $dados->getDescricao(); ?>" required>
			</div>
		</div><br>

		<div class="row">
  			<div class="col-xs-6 col-md-3">
    			<a href="#" class="thumbnail">
      				<img src="uploads/servicos/<?= $dados->getUrlIcone(); ?>" alt="imagem do Serviço">
	 	   		</a>
  			</div>  			
		</div>

		<div class="row">
			<div class="col-md-6">
				<label>Url</label>
				<input type="file" name="url_icone" id="url_icone" required>
				<input type="hidden" name="urlOld" id="urlOld" value="<?= $dados->getUrlIcone(); ?>">							
			</div>
		</div><br>	

		<div class="row">
			<div class="col-md-6">
				<label>Home</label>
				<input type="text" class="form-control" id="home" name="home" required value="<?= $dados->getHome(); ?>">
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-2">
				<button class="btn btn-warning form-control"><b><i class="glyphicon glyphicon-edit"></i>Editar</b></button>
			</div>&nbsp;

			<div class="col-md-2">
				<a class="btn btn-danger form-control" href="index.php?route=listServicos"><b><i class="glyphicon glyphicon-remove"></i>&nbsp;Cancelar</b></a>
			</div>&nbsp;			
		</div>		
	</form>

</div>