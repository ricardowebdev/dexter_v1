<?php

	namespace Controller\Banners;

	use src\Conexao\Conexao as Conexao;
	use Model\Banners\Banner as Banner;
	use src\Traits\MensagemTrait;
	use src\View\View;	

	class BannerController
	{

		public function __construct()
		{
			
		}

		public static function listBanners($pdo)
		{
			$prepare = $pdo->prepare("SELECT * FROM banners");
			$prepare->execute();
			$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);
			$banners = [];

			foreach ($result as $dados) {
				$banner = new Banner();
				$banner->setId($dados['id']);
				$banner->setNome($dados['nome']);
				$banner->setDescricao($dados['descricao']);
				$banner->setUrl($dados['url']);

				$banners[] = $banner;
			}

			View::mountPage('Banner', 'Listar', $banners);			
		}

        public static function deleteBanner($pdo)
		{
            if(isset($_GET['id']) && !empty($_GET['id'])) {				
                $prepare = $pdo->prepare("DELETE FROM banners WHERE id = ".$_GET['id']);

                if($prepare->execute()) 
                    MensagemTrait::set("Banner removido com sucesso", "success");
                else 
                    MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
            }

            View::redirect('listBanners');										
		}

        public static function selectBanner($pdo, $id)
		{
			$prepare = $pdo->prepare("SELECT * FROM banners WHERE id = ".$id);
			$prepare->execute();
			$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);

			foreach ($result as $dados) {				
				$folder = $dados['url'];

				$banner = new Banner();
				$banner->setId($dados['id']);
				$banner->setNome($dados['nome']);
				$banner->setDescricao($dados['descricao']);
				$banner->setUrl($folder);				
			}	

			View::mountPage('Banner', 'Editar', $banner);			
		}

        public static function updateBanner($pdo, $dados)
		{
			if (isset($_POST['id']) && !empty($_POST['id'])) {
				
				$banner = new Banner;


				// Cuidando da atualização da foto
				if (isset($_FILES['url']) && !empty($_FILES['url'])) {
					if ($banner->removeBannerOld($_POST['urlOld'])) {

						$url = $banner->uploadBanner();

						if ($url['status'] == "error") {
							MensagemTrait::set($url['msg'], "danger");
							View::redirect('listBanners');				
						} else {
							$dados['url'] = $url['msg'];
						}

					} else {
						MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
						View::redirect('listBanners');
					}
				} else {
					$dados['url'] = $dados['urlOld'];
				}


				$sql = "UPDATE banners SET nome = :nome,
						                  descricao = :descricao,
						                  url = :url
						WHERE id = :id";

				$prepare = $pdo->prepare($sql);
				$prepare->bindValue(":nome",      $dados['nome']);
				$prepare->bindValue(":descricao", $dados['descricao']);
				$prepare->bindValue(":url", 	  $dados['url']);
				$prepare->bindValue(":id", 		  $dados['id']);						

				if($prepare->execute()) {
					MensagemTrait::set("Banner alterado com sucesso", "success");
				} else {
					MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
				}	

				View::redirect('listBanners');				
			} else {
				self::selectBanner($pdo, $_GET['id']);				
			}
					
		}

        public static function insertBanner($pdo, $dados)
		{
			if (isset($_POST['nome']) && !empty($_POST['nome'])) {
				$banner   = new Banner;

				// Realizando o upload da foto
				$url = $banner->uploadBanner();

				if ($url['status'] == "error") {
					MensagemTrait::set($validate, "danger");
					View::redirect('newBanner');				
				} else {
					$dados['url'] = $url['msg'];
				}

				//Validando os Dados			
				$validate = $banner->validaDados($dados);

				if ($validate != "true") {
					MensagemTrait::set($validate, "danger");
					View::redirect('newBanner');
				} else {

					$sql = "INSERT INTO banners (nome,
												 descricao,
												 url)
										 VALUES (:nome,
										 		 :descricao,
										 		 :url)";

					$prepare = $pdo->prepare($sql);
					$prepare->bindValue(":nome",      $dados['nome']);
					$prepare->bindValue(":descricao", $dados['descricao']);
					$prepare->bindValue(":url", 	  $dados['url']);

					if($prepare->execute()) {
						MensagemTrait::set("Banner inserido com sucesso", "success");
					} else {
						MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
					}				

					View::redirect('listBanners');							
				}	

			} else {
				View::mountPage('Banner', 'Cadastrar');
			}
		}
	}