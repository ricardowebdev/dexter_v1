
<div class="title">
	<h1>Vantagens</h1>
	<h3>Uma plataforma moderna e flexivel para gerenciar a logistica do seu
		negócio.</h3>
</div>

<ul class="container">
    <?php 
        $prepare = $pdo->prepare("SELECT * FROM vantagens");
        $prepare->execute();
        $vantagens = $prepare->fetchAll(PDO::FETCH_ASSOC);                
    ?>
    <?php foreach ($vantagens as $vantagem) : ?>
	<li>
		<figure>
			<img src="http://ricardowebdev.com.br/dexter/uploads/vantagens/<?= $vantagem['url_icone'] ;?>" alt="Nome Imagem">
			<figcaption>
				<strong><?= $vantagem['nome'] ;?></strong>
				<p><?= $vantagem['descricao'] ;?></p>
			</figcaption>
		</figure>
	</li>
    <?php endforeach; ?>
</ul>

<div class="clear"></div>

<a href="servicos.php" class="btn btn-flat">Veja mais Vantagens</a>
