$(document).ready(function(){	
	$('#table').DataTable({
	    "language": {
	        "url": "src/assets/js/datatable_pt.json"
	    },
	    "pageLength": 50
	});		  

	$('.cpf').mask('000.000.000-00', {reverse: true});      
	$('.cnpj').mask('00.000.000/0000-00', {reverse: true});  
    $('.cep').mask('00000-000');
    $('.phone').mask('(00) 0 0000-0000');
    $('.phoneFix').mask('(00) 0000-0000');
    $('.cnpj').mask('00.000.000/0000-00', {reverse: true});

    $('#cep').focusout(function(){
    	var cep = $('#cep').val();

	    $.ajax({
	        type : "GET",    
	        url  : 'https://viacep.com.br/ws/' + cep + '/json/', 
	        dataType: 'json'	        
	    }).done(function(data) {
	    	$('#cidade').val(data.localidade);
	    	$('#bairro').val(data.bairro);
	    	$('#endereco').val(data.logradouro);
	    	$('#estado').val(data.uf);
	    });
    });  
});

function callDelete(page, id)
{
	$('#mTitle').html('Exclusão de ' + page);
	$('#mBody').html('Tem certeza de que deseja excluir esse ' + page + ' ?');
	$('#mConfirm').attr("href", "index.php?route=delete" + page + "&id="+id);	
}
