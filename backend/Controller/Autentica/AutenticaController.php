<?php 
	namespace Controller\Autentica;

	use src\Conexao\Conexao as Conexao;
	use src\Traits\MensagemTrait;
	use src\View\View;	

	class AutenticaController
	{

		public function __construct()
		{

		}

		public static function autentica($pdo)
		{					

			if(!isset($_SESSION['tkLogged'])) {	

				$nome  = isset($_POST['nome'])  ? $_POST['nome']       : '';
				$senha = isset($_POST['senha']) ? md5($_POST['senha']) : '';				
				
				$sql = "SELECT * FROM funcionarios
					    WHERE nome = :nome
					    AND senha = :senha";

				$prepare = $pdo->prepare($sql);
				$prepare->bindValue(":nome",  $nome);
				$prepare->bindValue(":senha", $senha);
				$prepare->execute();
				$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);

				$senha = "";				

				foreach($result as $dados) {
					$senha = $dados['senha'];
				}

				if($senha != "") {
					$_SESSION['tkLogged'] = self::generateToken($senha);
					return TRUE;
				} else {
					session_destroy();
					session_start();
					MensagemTrait::set("Usuario e ou senha incorretos", "danger");					
					View::notLogged();					
				}				
			} else {
				return TRUE;
			}
		}

		public static function generateToken($senha)
		{
			$token = password_hash($senha, PASSWORD_DEFAULT);
			return $token;
		}

		public static function logoff()
		{
			session_destroy();			
			View::notLogged();
		}
	}
	
	

	