<br><div class="container">
	<div class="row">
		<div class="col-md-3">&nbsp;</div>
		<div class="col-md-6 center"><h1><b>Novo Funcionario</b></h1></div>
		<div class="col-md-3">&nbsp;</div>
	</div><hr>

	<form method="POST" action="index.php?route=newFuncionario">	
		<input type="hidden" name="id">
		<div class="row">
			<div class="col-md-6">
				<label>Perfil</label>
				<select type="text" name="prf_id" id="prf_id" class="form-control" required>
					<option value="">Selecione</option>
					<option value="1">Administrador</option>
					<option value="2">Funcionario</option>
				</select>

			</div>
		</div><br>

		<div class="row">
			<div class="col-md-6">
				<label>Nome</label>
				<input type="text" name="nome" id="nome" placeholder="Nome do funcionario" class="form-control" required>
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-6">
				<label>Email</label>
				<input type="text" name="email" id="email" placeholder="funcioanrio@mail.com" class="form-control" required>
			</div>
		</div><br>	

		<div class="row">
			<div class="col-md-6">
				<label>Senha</label>
				<input type="password" name="senha" id="senha" placeholder="Digite sua senha" class="form-control" required>
			</div>
		</div><br>	

		<div class="row">
			<div class="col-md-2">
				<button class="btn btn-success form-control"><b><i class="glyphicon glyphicon-plus"></i>Cadastrar</b></button>
			</div>&nbsp;

			<div class="col-md-2">
				<a class="btn btn-danger form-control" href="index.php?route=listFuncionarios"><b><i class="glyphicon glyphicon-remove"></i>&nbsp;Cancelar</b></a>
			</div>&nbsp;			
		</div>
	</form>

</div>