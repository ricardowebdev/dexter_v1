-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: mysql796.umbler.com    Database: doesemfronteiras
-- ------------------------------------------------------
-- Server version	5.6.40-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `senha` varchar(100) COLLATE utf8_bin NOT NULL,
  `token` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (14,'mario@bros.com.br','$2y$10$X7tq3i628Qfbw7ZAEo1FkuTkOauHBAe1mykW4JtYDB83an0MLaI4e','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJyaWNhcmRvRGV2LmNvbS5iciIsImlkVXNlciI6IjE0IiwiZW1haWwiOiJtYXJpb0Bicm9zLmNvbS5iciJ9.gK1E9yuWmA9HFek0T5k4HwgucKJLXExz4HPn/+KGhwU='),(13,'ricardo.tecnology@gmail.com','$2y$10$336eKozVzcBKymEjDiBM4eYj1cllHQ35G.9C09dVTQlj9TtKsyFqy','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJyaWNhcmRvRGV2LmNvbS5iciIsImlkVXNlciI6IjEzIiwiZW1haWwiOiJyaWNhcmRvLnRlY25vbG9neUBnbWFpbC5jb20ifQ==.8e0BTfiTuAxT9308c4OmN5rxKEfH0xadwoHhzWypodo='),(15,'bruce@wayne.com','$2y$10$qKDx8nL1G2cvekSKoktOVeJjcD7mwub2lKCXHQUIC3a5PQooS5uQG','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJyaWNhcmRvRGV2LmNvbS5iciIsImlkVXNlciI6IjE1IiwiZW1haWwiOiJicnVjZUB3YXluZS5jb20ifQ==.owx85crWuYSlx+5GeefBgLCZgCgHB/F8lgH0bACMXl0='),(17,'wellington@ton.com','$2y$10$JCtr.HzSGy/Y8.JzIqmc8.41d2nGTBB9fFgXGh0finxWHsbsZHq5C','NI');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-23  9:54:35
