<?php
	namespace Model\Vantagens;

	class Vantagem
	{
		private $id;
		private $nome;
		private $descricao;
		private $url_icone;

		public function __construct()
		{

		}


		public function setNome($nome)
		{
			$this->nome = $nome;
		}

		public function getNome()
		{
			return $this->nome;
		}


		public function setDescricao($descricao)
		{
			$this->descricao = $descricao;
		}

		public function getDescricao()
		{
			return $this->descricao;
		}
			

		public function setUrlIcone($url)
		{
			$this->url = $url;
		}

		public function getUrlIcone()
		{
			return $this->url;
		}


		public function getId()
		{
			return $this->id;
		}

		public function setId($id)
		{
			$this->id = $id;
		}

		public function isNew()
		{
			return $this->id ? true : false;
		}

		public function validaDados($dados)
		{
			if (!isset($dados['nome']) || empty($dados['nome'])) {
				return "Nome da Vantagem não pode ficar em branco";
			} elseif (!isset($dados['descricao']) || empty($dados['descricao'])) {
				return "Descrição da Vantagem não pode ficar em branco";
			} elseif (!isset($_FILES['url_icone']) || empty($_FILES['url_icone'])) {
				return "Imagem da Vantagem não pode ficar em branco";
			} else {
				return "true";
			}
		}

		public function uploadVantagem()
		{	        	
			//Pegando extensão do arquivo
	        $ext = strtolower(substr($_FILES['url_icone']['name'],-4)); 

			if($ext != '.jpg' && $ext != '.png' && $ext != '.jpe') {
				return $result = array("status" => "error",
									   "msg"    => "Extensão do Arquivo não é valida");
			} else {
		        $new_name = date("Y.m.d-H.i.s").$ext;
		        $dir = 'uploads/vantagens/'; 
				
				//Fazer upload do arquivo
		        if(move_uploaded_file($_FILES['url_icone']['tmp_name'], $dir.$new_name)) {
					return $result = array("status" => "success",
										   "msg"    => $new_name);
		        } else {
		        	return $result = array("status" => "error",
									   	   "msg"    => "Ocorreram Erros durante o upload da foto");
		        }			        
			} 
		}

		public function removeVantagemOld($url)
		{
			$dir = 'uploads/vantagens/'.$url;

			if (unlink($dir)) {
				return "true";
			} else {
				return "false";
			}

		}

	}