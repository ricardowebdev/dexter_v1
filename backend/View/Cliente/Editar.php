<br><div class="container">
	<div class="row">
		<div class="col-md-3">&nbsp;</div>
		<div class="col-md-6 center"><h1><b>Edição de cliente</b></h1></div>
		<div class="col-md-3">&nbsp;</div>
	</div><hr>

	<form method="POST" action="index.php?route=editCliente">	
		<input type="hidden" name="id" value="<?= $dados->getId(); ?>">
		<div class="row">
			<div class="col-md-6">
				<label>Nome / Razão</label>
				<input type="text" name="nome_razao" id="nome_razao" placeholder="Nome do cliente" class="form-control" required value="<?= $dados->getNomeRazao(); ?>">
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-6">
				<label>Email</label>
				<input type="email" name="email" id="email" placeholder="seu@email.com.br" class="form-control" required value="<?= $dados->getEmail(); ?>">
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-3">
				<label>Telefone</label>
				<input type="text" name="telefone" id="telefone" placeholder="(XX) XXXX-XXXX" class="form-control phoneFix" required value="<?= $dados->getTelefone(); ?>">
			</div>

			<div class="col-md-3">
				<label>Celular</label>
				<input type="text" name="celular" id="celular" placeholder="(XX) X XXXX-XXXX" class="form-control phone" required value="<?= $dados->getCelular(); ?>">
			</div>
		</div><br>	

		<div class="row">
			<div class="col-md-3">
				<label>Cep</label>
				<input type="text" name="cep" id="cep" placeholder="00000-000" class="form-control cep" required value="<?= $dados->getCep(); ?>">
			</div>

			<div class="col-md-6">
				<label>Endereco</label>
				<input type="text" name="endereco" id="endereco" placeholder="Digite seu endereço" class="form-control" required value="<?= $dados->getEndereco(); ?>">
			</div>
		</div><br>			

		<div class="row">
			<div class="col-md-3">
				<label>Estado</label>
				<select type="text" name="estado" id="estado" class="form-control" required>
					<option value="<?= $dados->getEstado(); ?>"><?= $dados->getEstado(); ?></option>
					<option value="">Selecione</option>					
					<option value="SP">SP</option>
					<option value="RJ">RJ</option>
					<option value="MG">MG</option>
					<option value="SC">SC</option>
					<option value="RS">RS</option>
					<option value="PR">PR</option>
					<option value="BA">BA</option>
					<option value="PE">PE</option>
					<option value="ES">ES</option>
				</select>
			</div>

			<div class="col-md-4">
				<label>Cidade</label>
				<input type="text" name="cidade" id="cidade" placeholder="Digite sua cidade" class="form-control" required value="<?= $dados->getCidade(); ?>">
			</div>			

			<div class="col-md-4">
				<label>Bairro</label>
				<input type="text" name="bairro" id="bairro" placeholder="Digite seu bairro" class="form-control" required value="<?= $dados->getBairro(); ?>">
			</div>			

		</div><br>			

		<div class="row">
			<div class="col-md-2">
				<button class="btn btn-warning form-control"><b><i class="glyphicon glyphicon-edit"></i>&nbsp;Editar</b></button>
			</div>&nbsp;

			<div class="col-md-2">
				<a class="btn btn-danger form-control" href="index.php?route=listClientes"><b><i class="glyphicon glyphicon-remove"></i>&nbsp;Cancelar</b></a>
			</div>&nbsp;			
		</div>
	</form>

</div>