<?php
	session_start();
	date_default_timezone_set("Brazil/East"); 

	include "autoload.php";

	// Use dos Src
	use src\Conexao\Conexao;	
	use src\View\View;
	
	// Use dos Controllers
	use Controller\Banners\BannerController;
	use Controller\Clientes\ClienteController;
	use Controller\Funcionalidades\FuncionalidadeController;
	use Controller\Funcionarios\FuncionarioController;
	use Controller\Servicos\ServicoController;
	use Controller\Autentica\AutenticaController;
	use Controller\Vantagens\VantagemController;

	$pdo   = Conexao::getInstance();
	$route = !isset($_GET['route']) ? 'home' : $_GET['route'];

	AutenticaController::autentica($pdo);

	switch ($route) {
		case 'home':
			View::mountPage('Home', 'Home');						
			break;	
		case 'logoff':
			AutenticaController::logoff();
			break;			
		// ----------- Rotas do banner ------------- //
		case 'listBanners':	
			//$view = new View('BannerController', 'listBanners2', 'mountPage2', 'Banner', 'Listar');			
            BannerController::listBanners($pdo);						
			break;
		case 'deleteBanner':
            BannerController::deleteBanner($pdo);						
			break;
		case 'editBanner':			
			BannerController::updateBanner($pdo, $_POST);	
			break;	
		case 'newBanner' :							
			BannerController::insertBanner($pdo, $_POST);
			break;
		// --------------- Fim do Banner ------------- //

		// ----------- Rotas das vantagens ------------- //
		case 'listVantagens':	
            VantagemController::listVantagens($pdo);						
			break;
		case 'deleteVantagem':
            VantagemController::deleteVantagem($pdo);						
			break;
		case 'editVantagem':			
			VantagemController::updateVantagem($pdo, $_POST);	
			break;	
		case 'newVantagem' :							
			VantagemController::insertVantagem($pdo, $_POST);
			break;
		// --------------- Fim das Vantagens ------------- //			

		// ------------- Rotas do Cliente ------------ //
		case 'listClientes':
			ClienteController::listClientes($pdo);			
			break;
		case 'deleteCliente':
            ClienteController::deleteCliente($pdo);						
			break;
		case 'editCliente':			
			ClienteController::updateCliente($pdo, $_POST);
			break;	
		case 'newCliente' :							
			ClienteController::insertCliente($pdo, $_POST);
			break;			
		// ------------- Fim do Cliente -------------- //	

		// ------- Rotas das Funcionalidades --------- //
		case 'listFuncionalidades':
			FuncionalidadeController::listFuncionalidades($pdo);
			break;
		case 'deleteFuncionalidade':
            FuncionalidadeController::deleteFuncionalidade($pdo);	
			break;
		case 'editFuncionalidade':			
			FuncionalidadeController::updateFuncionalidade($pdo, $_POST);
			break;	
		case 'newFuncionalidade' :							
			FuncionalidadeController::insertFuncionalidade($pdo, $_POST);
			break;			
		// -------- Fim das Funcionalidades ---------- //

		// --------- Rotas dos Funcionarios ---------- //
		case 'listFuncionarios':
			FuncionarioController::listFuncionarios($pdo);
			break;
		case 'deleteFuncionario':
            FuncionarioController::deleteFuncionario($pdo);
			break;
		case 'editFuncionario':			
			FuncionarioController::updateFuncionario($pdo, $_POST);
			break;	
		case 'newFuncionario' :							
			FuncionarioController::insertFuncionario($pdo, $_POST);	
			break;				
		// ---------- Fim dos Funcionarios ----------- //

		// ------------ Rotas dos Servicos ----------- //			
		case 'listServicos':
			ServicoController::listServicos($pdo);
			break;
		case 'deleteServico':
            ServicoController::deleteServico($pdo);						
			break;
		case 'editServico':			
			ServicoController::updateServico($pdo, $_POST);
			break;	
		case 'newServico' :							
			ServicoController::insertServico($pdo, $_POST);		
			break;			
		// ------------ Fim dos Serviços ------------- //
		default:
			AutenticaController::logoff();
			break;
	}