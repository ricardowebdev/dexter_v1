<?php
	namespace Controller\Servicos;

	use src\Conexao\Conexao as Conexao;
	use Model\Servicos\Servico as Servico;
	use src\Traits\MensagemTrait;
	use src\View\View;	

	class ServicoController
	{
		public static function listServicos($pdo)
		{
			$prepare = $pdo->prepare("SELECT * FROM servicos");
			$prepare->execute();
			$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);
			$servicos = [];

			foreach ($result as $dados) {
				$servico = new Servico();
				$servico->setId($dados['id']);
				$servico->setNome($dados['nome']);
				$servico->setDescricao($dados['descricao']);
				$servico->setUrlIcone($dados['url_icone']);
				$servico->setHome($dados['home']);

				$servicos[] = $servico;
			}

			View::mountPage('Servico', 'Listar', $servicos);
		}

		public static function updateServico($pdo, $dados)
		{
			if(isset($_POST['id']) && !empty($_POST['id'])) {

				$servico = new Servico;



				// Cuidando da atualização da foto
				if (isset($_FILES['url_icone']) && !empty($_FILES['url_icone'])) {
					if ($servico->removeServicoOld($_POST['urlOld'])) {

						$url = $servico->uploadServico();
									

						if ($url['status'] == "error") {
							MensagemTrait::set($url['msg'], "danger");
							View::redirect('listServicos');
						} else {
							$dados['url_icone'] = $url['msg'];
						}

					} else {
						MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
						View::redirect('listServicos');
					}
				} else {
					$dados['url_icone'] = $dados['urlOld'];
				}

				//Validando os Dados			
				$validate = $servico->validaDados($dados);

				if ($validate != "true") {
					MensagemTrait::set($validate, "danger");
					View::redirect('newServico');
				} else {								

					$sql = "UPDATE servicos SET nome = :nome,
							                    descricao = :descricao,
							                    url_icone = :url_icone,
							                    home = :home
							WHERE id = :id";

					$prepare = $pdo->prepare($sql);
					$prepare->bindValue(":nome", 	  $dados['nome']);
					$prepare->bindValue(":descricao", $dados['descricao']);
					$prepare->bindValue(":url_icone", $dados['url_icone']);
					$prepare->bindValue(":home",      $dados['home']);
					$prepare->bindValue(":id", 	      $dados['id']);						

					if($prepare->execute()) {
						MensagemTrait::set("Serviço alterado com sucesso", "success");
					} else {
						MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
					}	

					View::redirect('listServicos');		
				}							
			} else {
				self::selectServico($pdo, $_GET['id']);				
			}
		}

		public static function deleteServico($pdo)
		{
            if(isset($_GET['id']) && !empty($_GET['id'])) {				
                $prepare = $pdo->prepare("DELETE FROM servicos WHERE id = ".$_GET['id']);

                if($prepare->execute()) 
                    MensagemTrait::set("Serviço removido com sucesso", "success");
                else 
                    MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
            }

            View::redirect('listServicos');
		}

		public static function insertServico($pdo, $dados)
		{
			if(isset($dados['nome']) && !empty($dados['nome'])) {
				$servico = new Servico;

				// Realizando o upload da foto
				$url = $servico->uploadServico();

				if ($url['status'] == "error") {
					MensagemTrait::set($url['msg'], "danger");
					View::redirect('newServico');				
				} else {
					$dados['url_icone'] = $url['msg'];
				}

				//Validando os Dados			
				$validate = $servico->validaDados($dados);

				if ($validate != "true") {
					MensagemTrait::set($validate, "danger");
					View::redirect('newServico');
				} else {					

					$sql = "INSERT INTO servicos (nome,
												  descricao,
												  url_icone,
												  home)
										  VALUES (:nome,
										 	  	  :descricao,
										 		  :url_icone,
										 		  :home)";

					$prepare = $pdo->prepare($sql);
					$prepare->bindValue(":nome", 	  $dados['nome']);
					$prepare->bindValue(":descricao", $dados['descricao']);
					$prepare->bindValue(":url_icone", $dados['url_icone']);			
					$prepare->bindValue(":home",      $dados['home']);			

					if($prepare->execute()) {
						MensagemTrait::set("Serviço inserido com sucesso", "success");
					} else {
						MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
					}				

					View::redirect('listServicos');	
				}						
			} else {
				View::mountPage('Servico', 'Cadastrar');
			}	
		}

		public static function selectServico($pdo, $id)
		{
			$prepare = $pdo->prepare("SELECT * FROM servicos WHERE id = ".$id);
			$prepare->execute();
			$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);
			$servico;

			foreach ($result as $dados) {
				$servico = new Servico();
				$servico->setId($dados['id']);
				$servico->setNome($dados['nome']);
				$servico->setDescricao($dados['descricao']);
				$servico->setUrlIcone($dados['url_icone']);
				$servico->setHome($dados['home']);
			}	

			View::mountPage('Servico', 'Editar', $servico);				
		}
	}