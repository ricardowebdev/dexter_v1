	<br><div class="container-fluid">
		<div class="row">
			<div class="col-md-3">&nbsp;</div>
			<div class="col-md-6 center"><h1><b>Funcionalidades</b></h1></div>
			<div class="col-md-3">&nbsp;</div>
		</div><br>

		<div class="row">
			<div class="col-md-2">
				<a href="index.php?route=newFuncionalidade" class="btn btn-primary">
					<b><i class="glyphicon glyphicon-plus"></i>&nbsp;Adicionar</b>
				</a>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12" style="padding: 25px;">
				<table class="table table-striped" id="table">
				    <thead>
				        <tr>
				            <th>#</th>
				            <th>Nome</th>
				            <th>Descricao</th>
				            <th>Url</th>
				            <th>Acoes</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($dados as $funcionalidade) : ?>
				    		<tr>	
					        	<th scope="row"><?= $funcionalidade->getId(); ?></th>		         
					            <td><?= $funcionalidade->getNome(); ?></td>
					            <td><?= $funcionalidade->getDescricao(); ?></td>
					            <td><?= $funcionalidade->getUrlIcone(); ?></td>
					            <td>
					            	<a href="index.php?route=editFuncionalidade&id=<?= $funcionalidade->getId(); ?>" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-pencil"></i></a>
					            	<a onclick="callDelete('Funcionalidade', <?= $funcionalidade->getId(); ?>)" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal"><i class="glyphicon glyphicon-trash"></i></a>
					            </td>
					        </tr>
					    <?php endforeach ?>    
	  			    </tbody>
				</table>				
			</div>
		</div>
	</div>