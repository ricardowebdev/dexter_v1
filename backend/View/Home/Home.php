<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 center"><h2>Dashboard</h2></div>
	</div><br>
	<div class="row">
		<div class="col-md-6">
			<canvas id="barChart" width="100%" height="50px"></canvas>			
		</div>
		<div class="col-md-6">
			<canvas id="pieChart" width="100%" height="50px"></canvas>			
		</div>
	</div><br><br>
	<div class="row">
		<div class="col-md-6">
			<canvas id="doughnutChart" width="100%" height="50px"></canvas>						
		</div>
	</div>
</div>

