<?php
	namespace Model\Funcionarios;

	class Funcionario
	{
		private $id;
		private $idPrf;
		private $nome;
		private $email;
		private $senha;
		private $profile;

		public function __construct()
		{

		}

		public function setIdPrf($idPrf)
		{
			$this->idPrf = $idPrf;
			$this->profile = $idPrf == 1 ? "Administrador" : "Funcionario";
		}

		public function getIdPrf()
		{
			return $this->idPrf;
		}

		public function getProfile()
		{
			return $this->profile;
		}

		public function setNome($nome)
		{
			$this->nome = $nome;
		}

		public function getNome()
		{
			return $this->nome;
		}


		public function setEmail($email)
		{
			$this->email = $email;
		}

		public function getEmail()
		{
			return $this->email;
		}
			

		public function setSenha($senha)
		{
			$this->senha = $senha;
		}

		public function getSenha()
		{
			return $this->senha;
		}


		public function getId()
		{
			return $this->id;
		}

		public function setId($id)
		{
			$this->id = $id;
		}

		public function isNew()
		{
			return $this->id ? true : false;
		}

	}