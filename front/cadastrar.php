<?php
	if(isset($_POST['nome_razao']) && !empty($_POST['nome_razao'])) {

		$validacao = validaDados($_POST);

		if($validacao) {
			session_start();
			include 'include/banco.php';
			$pdo = Conexao::getInstance();

			$sql = "INSERT INTO clientes (nome_razao,
										  email,
										  telefone,
										  celular,
										  cep,
										  endereco,
										  estado,
										  cidade,
										  bairro)
								 VALUES (:nome_razao,
								 		 :email,
										 :telefone,
										 :celular,
										 :cep,
										 :endereco,
										 :estado,
										 :cidade,
										 :bairro)";


			$prepare = $pdo->prepare($sql);
			$prepare->bindValue(":nome_razao", $_POST['nome_razao']);
			$prepare->bindValue(":email",      $_POST['email']);
			$prepare->bindValue(":telefone",   $_POST['telefone']);
			$prepare->bindValue(":celular",    $_POST['celular']);
			$prepare->bindValue(":cep",        $_POST['cep']);
			$prepare->bindValue(":endereco",   $_POST['endereco']);
			$prepare->bindValue(":estado",     $_POST['estado']);
			$prepare->bindValue(":cidade",     $_POST['cidade']);
			$prepare->bindValue(":bairro",     $_POST['bairro']);

			if($prepare->execute()) {
				$_SESSION['status'] = 'success';
				$_SESSION['msg']    = 'Cadastro Realizado com Sucesso';
			} else {
				$_SESSION['status'] = 'danger';
				$_SESSION['msg']    = 'Ocorreram Erros durante a solicitação';
			}

			header('location: cadastro.php');

		} else {
			$_SESSION['msg']    = $validacao;
			$_SESSION['status'] = 'danger';
			header('location: cadastro.php');
		}
	} else {
		header('location: cadastro.php');
	}

	function validaDados($dados)
	{
		if(!isset($dados['nome_razao']) || empty($dados['nome_razao'])) {

			$validacao = 'Nome / Razão não pode ficar em branco';

		} else if (!isset($dados['email']) || empty($dados['email'])) {

			$validacao = 'E-mail não pode ficar em branco';

		} else if (!isset($dados['telefone']) || empty($dados['telefone'])) {

			$validacao = 'Telefone não pode ficar em branco';

		} else if (!isset($dados['celular']) || empty($dados['celular'])) {

			$validacao = 'Celular não pode ficar em branco';

		} else if (!isset($dados['cep']) || empty($dados['cep'])) {

			$validacao = 'Cep não pode ficar em branco';

		} else if (!isset($dados['endereco']) || empty($dados['endereco'])) {

			$validacao = 'Endereço não pode ficar em branco';

		} else if (!isset($dados['estado']) || empty($dados['estado'])) {

			$validacao = 'Estado não pode ficar em branco';

		} else if (!isset($dados['cidade']) || empty($dados['cidade'])) {

			$validacao = 'Cidade não pode ficar em branco';

		} else if (!isset($dados['bairro']) || empty($dados['bairro'])) {
			
			$validacao = 'Bairro não pode ficar em branco';

	    } else {
			$validacao = true;
		} 

		return $validacao;
	}
?>