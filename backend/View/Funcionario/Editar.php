<br><div class="container">
	<div class="row">
		<div class="col-md-3">&nbsp;</div>
		<div class="col-md-6 center"><h1><b>Edição dos Funcionarios</b></h1></div>
		<div class="col-md-3">&nbsp;</div>
	</div><hr>

	<form method="POST" action="index.php?route=editFuncionario">	
		<input type="hidden" name="id"       value="<?= $dados->getId(); ?>">
		<input type="hidden" name="senhaOld" value="<?= $dados->getSenha(); ?>">
		<div class="row">
			<div class="col-md-6">
				<label>Perfil</label>
				<select type="text" name="prf_id" id="prf_id" placeholder="Nome" class="form-control" required>
					<option value="<?= $dados->getIdPrf(); ?>"><?= $dados->getProfile(); ?></option>
					<option value="1">Administrador</option>
					<option value="2">Funcionario</option>
				</select>
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-6">
				<label>Nome</label>
				<input type="text" name="nome" id="nome" placeholder="Nome" class="form-control" value="<?= $dados->getNome(); ?>" required>
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-6">
				<label>E-mail</label>
				<input type="mail" name="email" id="email" placeholder="e-mail" class="form-control" required value="<?= $dados->getEmail(); ?>">
			</div>
		</div><br>	

		<div class="row">
			<div class="col-md-6">
				<label>Senha</label>
				<input type="password" class="form-control" id="senha" name="senha" required value="<?= $dados->getSenha(); ?>">
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-2">
				<button class="btn btn-warning form-control"><b><i class="glyphicon glyphicon-edit"></i>&nbsp;Editar</b></button>
			</div>&nbsp;

			<div class="col-md-2">
				<a class="btn btn-danger form-control" href="index.php?route=listFuncionarios"><b><i class="glyphicon glyphicon-remove"></i>&nbsp;Cancelar</b></a>
			</div>&nbsp;			
		</div>		
	</form>

</div>