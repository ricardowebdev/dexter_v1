<?php
	require '../Conexao/Conexao.php';
	use src\Conexao\Conexao;	

	// Inicializando
	$clientes 		 = 0;
	$banners  		 = 0;
	$funcionalidades = 0;
	$funcionarios 	 = 0;
	$servicos 		 = 0;
	$administradores = 0;
	$usuarios 		 = 0;

	$pdo = Conexao::getInstance();

	// Clientes
	$prepare = $pdo->prepare("SELECT COUNT(id) AS clientes FROM clientes");
	$prepare->execute();
	$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);

	foreach ($result as $dados) {
		$clientes = $dados['clientes'];
	}

	// Banners
	$prepare = $pdo->prepare("SELECT COUNT(id) AS banners FROM banners");
	$prepare->execute();
	$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);

	foreach ($result as $dados) {
		$banners = $dados['banners'];
	}

	// Funcionalidades
	$prepare = $pdo->prepare("SELECT COUNT(id) AS funcionalidades FROM funcionalidades");
	$prepare->execute();
	$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);

	foreach ($result as $dados) {
		$funcionalidades = $dados['funcionalidades'];
	}


	// Funcionarios
	$sql = "SELECT COUNT(id) AS funcionarios, prf_id 
			FROM funcionarios
			GROUP BY prf_id";
	$prepare = $pdo->prepare($sql);
	$prepare->execute();
	$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);

	foreach ($result as $dados) {
		$funcionarios += $dados['funcionarios'];

		if($dados['prf_id'] == "1")
			$administradores += $dados['funcionarios'];
		else
			$usuarios += $dados['funcionarios'];
	}

	// Servicos
	$prepare = $pdo->prepare("SELECT COUNT(id) AS servicos FROM servicos");
	$prepare->execute();
	$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);

	foreach ($result as $dados) {
		$servicos = $dados['servicos'];
	}


	

	$dados = array('clientes'        => $clientes,
				   'banners'         => $banners,
				   'funcionalidades' => $funcionalidades,
				   'funcionarios'    => $funcionarios,
				   'servicos'        => $servicos,
				   'administradores' => $administradores,
				   'usuarios'        => $usuarios);

	echo json_encode($dados);