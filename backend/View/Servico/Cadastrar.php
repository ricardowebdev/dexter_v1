<br><div class="container">
	<div class="row">
		<div class="col-md-3">&nbsp;</div>
		<div class="col-md-6 center"><h1><b>Novo Serviço</b></h1></div>
		<div class="col-md-3">&nbsp;</div>
	</div><hr>

	<form method="POST" action="index.php?route=newServico" enctype="multipart/form-data">	
		<input type="hidden" name="id">
		<div class="row">
			<div class="col-md-6">
				<label>Nome</label>
				<input type="text" name="nome" id="nome" class="form-control" required placeholder="Nome">
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-6">
				<label>Descrição</label>
				<input type="text" name="descricao" id="descricao" placeholder="Descrição" class="form-control" required>
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-6">
				<label>Url</label>
				<input type="file" name="url_icone" id="url_icone" required>
			</div>
		</div><br>	

		<div class="row">
			<div class="col-md-6">
				<label>Home</label>
				<input type="text" name="home" id="home" placeholder="Home" class="form-control" required>
			</div>
		</div><br>	

		<div class="row">
			<div class="col-md-2">
				<button class="btn btn-success form-control"><b><i class="glyphicon glyphicon-plus"></i>Cadastrar</b></button>
			</div>&nbsp;

			<div class="col-md-2">
				<a class="btn btn-danger form-control" href="index.php?route=listServicos"><b><i class="glyphicon glyphicon-remove"></i>&nbsp;Cancelar</b></a>
			</div>&nbsp;			
		</div>
	</form>

</div>