<?php
	namespace Controller\Funcionalidades;

	use src\Conexao\Conexao as Conexao;
	use Model\Funcionalidades\Funcionalidade as Funcionalidade;
	use src\Traits\MensagemTrait;
	use src\View\View;	

	class FuncionalidadeController
	{
		public static function listFuncionalidades($pdo)
		{
			$prepare = $pdo->prepare("SELECT * FROM funcionalidades");
			$prepare->execute();
			$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);
			$funcionalidades = [];

			foreach ($result as $dados) {
				$funcionalidade = new Funcionalidade();
				$funcionalidade->setId($dados['id']);
				$funcionalidade->setNome($dados['nome']);
				$funcionalidade->setDescricao($dados['descricao']);
				$funcionalidade->setUrlIcone($dados['url_icone']);

				$funcionalidades[] = $funcionalidade;
			}

			View::mountPage('Funcionalidade', 'Listar', $funcionalidades);
		}

		public static function updateFuncionalidade($pdo, $dados)
		{
			if(isset($_POST['id']) && !empty($_POST['id'])) {

				$funcionalidade = new Funcionalidade;

				// Cuidando da atualização da foto
				if (isset($_FILES['url_icone']) && !empty($_FILES['url_icone'])) {
					if ($funcionalidade->removeFuncionalidadeOld($_POST['urlOld'])) {

						$url = $funcionalidade->uploadFuncionalidade();

						if ($url['status'] == "error") {
							MensagemTrait::set($url['msg'], "danger");
							View::redirect('listFuncionalidades');				
						} else {
							$dados['url_icone'] = $url['msg'];
						}

					} else {
						MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
						View::redirect('listFuncionalidades');
					}
				} else {
					$dados['url_icone'] = $dados['urlOld'];
				}

				//Validando os Dados			
				$validate = $funcionalidade->validaDados($dados);

				if ($validate != "true") {
					MensagemTrait::set($validate, "danger");
					View::redirect('newFuncionalidade');
				} else {
					$sql = "UPDATE funcionalidades SET nome = :nome,
							                    	   descricao = :descricao,
							                    	   url_icone = :url_icone
							WHERE id = :id";

					$prepare = $pdo->prepare($sql);
					$prepare->bindValue(":nome", 	  $dados['nome']);
					$prepare->bindValue(":descricao", $dados['descricao']);
					$prepare->bindValue(":url_icone",  $dados['url_icone']);
					$prepare->bindValue(":id", 		  $dados['id']);						

					if($prepare->execute()) {
						MensagemTrait::set("Funcionalidade alterada com sucesso", "success");
					} else {
						MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
					}	

					View::redirect('listFuncionalidades');	
				}			
			} else {
				self::selectFuncionalidade($pdo, $_GET['id']);				
			}
		}

		public static function deleteFuncionalidade($pdo)
		{
            if(isset($_GET['id']) && !empty($_GET['id'])) {				
                $prepare = $pdo->prepare("DELETE FROM funcionalidades WHERE id = ".$_GET['id']);

                if($prepare->execute()) 
                    MensagemTrait::set("Funcionalidade removida com sucesso", "success");
                else 
                    MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
            }

            View::redirect('listFuncionalidades');
		}

		public static function insertFuncionalidade($pdo, $dados = "")
		{
			if(isset($dados['nome']) && !empty($dados['nome'])) {

				$funcionalidade = new Funcionalidade;

				// Realizando o upload da foto
				$url = $funcionalidade->uploadFuncionalidade();

				if ($url['status'] == "error") {
					MensagemTrait::set($validate, "danger");
					View::redirect('newFuncionalidade');				
				} else {
					$dados['url_icone'] = $url['msg'];
				}

				//Validando os Dados			
				$validate = $funcionalidade->validaDados($dados);

				if ($validate != "true") {
					MensagemTrait::set($validate, "danger");
					View::redirect('newFuncionalidade');
				} else {				

					$sql = "INSERT INTO funcionalidades (nome,
												  		 descricao,
												  		 url_icone)
										         VALUES (:nome,
										 		 		 :descricao,
										 		 		 :url_icone)";

					$prepare = $pdo->prepare($sql);
					$prepare->bindValue(":nome", 	  $dados['nome']);
					$prepare->bindValue(":descricao", $dados['descricao']);
					$prepare->bindValue(":url_icone", $dados['url_icone']);			

					if($prepare->execute()) {
						MensagemTrait::set("Funcionalidade inserida com sucesso", "success");
					} else {
						MensagemTrait::set("Ocorreram erros durante a solicitacao", "danger");
					}				

					View::redirect('listFuncionalidades');
				}
							
			} else {
				View::mountPage('Funcionalidade', 'Cadastrar');
			}		
		}

		public static function selectFuncionalidade($pdo, $id)
		{
			$prepare = $pdo->prepare("SELECT * FROM funcionalidades WHERE id = ".$id);
			$prepare->execute();
			$result = $prepare->fetchAll(\PDO::FETCH_ASSOC);
			$funcionalidade;

			foreach ($result as $dados) {
				$funcionalidade = new Funcionalidade();
				$funcionalidade->setId($dados['id']);
				$funcionalidade->setNome($dados['nome']);
				$funcionalidade->setDescricao($dados['descricao']);
				$funcionalidade->setUrlIcone($dados['url_icone']);				
			}	

			View::mountPage('Funcionalidade', 'Editar', $funcionalidade);			
		}
	}