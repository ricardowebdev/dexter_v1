<?php
	namespace Model\FaleConosco

	class FaleConosco
	{
		private $id;
		private $nome;
		private $assunto;
		private $mensagem;
		private $email;

		public function __construct()
		{

		}

		public function setNome($nome)
		{
			$this->nome = $nome
		}

		public function getNome()
		{
			return $this->nome;
		}


		public function setAssunto($assunto)
		{
			$this->assunto = $assunto
		}

		public function getAssunto()
		{
			return $this->assunto;
		}
			

		public function setMensagem($mensagem)
		{
			$this->mensagem = $mensagem
		}

		public function getMensagem()
		{
			return $this->mensagem;
		}

		public function setEmail($email)
		{
			$this->email = $email
		}

		public function getEmail()
		{
			return $this->email;
		}


		public function getId()
		{
			return $this->id;
		}


		public function isNew()
		{
			return $this->id ? true : false;
		}

	}