<?php
	namespace src\View;
	use src\Conexao\Conexao;	

	use Controller\Banners\BannerController;

	class View
	{		
		CONST HEADER = 'View/header.php';
		CONST FOOTER = 'View/footer.php';
		CONST MODAL  = 'View/modal.php';

		/*
		public function __construct($ctrl, $ctrlMtd, $metodo, $view, $arquivo)
		{
			$pdo   = Conexao::getInstance();
			$acao  = new $ctrl;
			$dados = $acao->{$ctrlMtd}($pdo);

			$this->{$metodo}($view, $arquivo, $dados);
		} */


		public static function mountPage($view, $arquivo, $dados = "")
		{
			include self::HEADER;
			include 'View/'.$view.'/'.$arquivo.'.php';
			include self::MODAL;
			include self::FOOTER;
		}

		/*
		public static function mountPage2($view, $arquivo, $dados)
		{			
			//Montando a pagina
			include self::HEADER;
			include 'View/'.$view.'/'.$arquivo.'.php';
			include self::MODAL;
			include self::FOOTER;			
		}
		*/

		public static function redirect($route)
		{
			header("location: index.php?route=$route");
		}

		public static function notLogged()
		{
			header("location: login.php");	
		}
	}