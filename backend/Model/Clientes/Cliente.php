<?php
	namespace Model\Clientes;

	class Cliente
	{
		private $id;
		private $nomeRazao;
		private $cpfCnpj;
		private $email;
		private $telefone;
		private $celular;
		private $cep;
		private $endereco;
		private $bairro;
		private $cidade;
		private $estado;


		public function __construct()
		{

		}


		public function setNomeRazao($nomeRazao)
		{
			$this->nomeRazao = $nomeRazao;
		}

		public function getNomeRazao()
		{
			return $this->nomeRazao;
		}


		public function setCpfCnpj($cpfCnpj)
		{
			$this->cpfCnpj = $cpfCnpj;
		}

		public function getCpfCnpj()
		{
			return $this->cpfCnpj;
		}
			

		public function setEmail($email)
		{
			$this->email = $email;
		}

		public function getEmail()
		{
			return $this->email;
		}


		public function setTelefone($telefone)
		{
			$this->telefone = $telefone;
		}

		public function getTelefone()
		{
			return $this->telefone;
		}


		public function setCelular($celular)
		{
			$this->celular = $celular;
		}

		public function getCelular()
		{
			return $this->celular;
		}


		public function setCep($cep)
		{
			$this->cep = $cep;
		}

		public function getCep()
		{
			return $this->cep;
		}


		public function setEndereco($endereco)
		{
			$this->endereco = $endereco;
		}

		public function getEndereco()
		{
			return $this->endereco;
		}



		public function setBairro($bairro)
		{
			$this->bairro = $bairro;
		}

		public function getBairro()
		{
			return $this->bairro;
		}


		public function setCidade($cidade)
		{
			$this->cidade = $cidade;
		}

		public function getCidade()
		{
			return $this->cidade;
		}		

		public function setEstado($estado)
		{
			$this->estado = $estado;
		}

		public function getEstado()
		{
			return $this->estado;
		}										


		public function setId($id)
		{
			$this->id = $id;
		}

		public function getId()
		{
			return $this->id;
		}


		public function isNew()
		{
			return $this->id ? true : false;
		}

	}