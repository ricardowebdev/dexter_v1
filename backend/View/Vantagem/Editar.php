<br><div class="container">
	<div class="row">
		<div class="col-md-3">&nbsp;</div>
		<div class="col-md-6 center"><h1><b>Edição das Vantagens</b></h1></div>
		<div class="col-md-3">&nbsp;</div>
	</div><hr>

	<form method="POST" action="index.php?route=editVantagem" enctype="multipart/form-data">	
		<input type="hidden" name="id" value="<?= $dados->getId(); ?>">
		<div class="row">
			<div class="col-md-6">
				<label>Nome</label>
				<input type="text" name="nome" id="nome" placeholder="Nome" class="form-control" value="<?= $dados->getNome(); ?>" required>
			</div>
		</div><br>

		<div class="row">
  			<div class="col-xs-6 col-md-3">
    			<a href="#" class="thumbnail">
      				<img src="uploads/vantagens/<?= $dados->getUrlIcone(); ?>" alt="imagem da Vantagem">
	 	   		</a>
  			</div>  			
		</div>

		<div class="row">
			<div class="col-md-6">
				<label>Url</label>
				<input type="file" name="url_icone" id="url_icone">	
				<input type="hidden" name="urlOld" id="urlOld" value="<?= $dados->getUrlIcone(); ?>">			
			</div>
		</div><br>		

		<div class="row">
			<div class="col-md-10">
				<label>Descricao</label>
				<textarea name="descricao" id="descricao" placeholder="Descricao" class="form-control" required><?= $dados->getDescricao(); ?></textarea>
			</div>
		</div><br>	

		<div class="row">
			<div class="col-md-2">
				<button class="btn btn-warning form-control"><b><i class="glyphicon glyphicon-edit"></i>Editar</b></button>
			</div>&nbsp;

			<div class="col-md-2">
				<a class="btn btn-danger form-control" href="index.php?route=listVantagens"><b><i class="glyphicon glyphicon-remove"></i>&nbsp;Cancelar</b></a>
			</div>&nbsp;						
		</div>		
	</form>

</div>