<?php
	namespace Model\Banners;

	class Banner
	{
		private $id;
		private $nome;
		private $descricao;
		private $url;

		public function __construct()
		{

		}


		public function setNome($nome)
		{
			$this->nome = $nome;
		}

		public function getNome()
		{
			return $this->nome;
		}


		public function setDescricao($descricao)
		{
			$this->descricao = $descricao;
		}

		public function getDescricao()
		{
			return $this->descricao;
		}
			

		public function setUrl($url)
		{
			$this->url = $url;
		}

		public function getUrl()
		{
			return $this->url;
		}


		public function getId()
		{
			return $this->id;
		}

		public function setId($id)
		{
			$this->id = $id;
		}

		public function isNew()
		{
			return $this->id ? true : false;
		}

		public function validaDados($dados)
		{
			if (!isset($dados['nome']) || empty($dados['nome'])) {
				return "Nome do Banner não pode ficar em branco";
			} elseif (!isset($dados['descricao']) || empty($dados['descricao'])) {
				return "Descrição do Banner não pode ficar em branco";
			} elseif (!isset($dados['url']) || empty($dados['url'])) {
				return "Imagem do Banner não pode ficar em branco";
			} else {
				return "true";
			}
		}

		public function uploadBanner()
		{	        	
			//Pegando extensão do arquivo
	        $ext = strtolower(substr($_FILES['url']['name'],-4)); 

			if($ext != '.jpg' && $ext != '.png' && $ext != '.jpe') {
				return $result = array("status" => "error",
									   "msg"    => "Extensão do Arquivo não é valida");
			} else {
		        $new_name = date("Y.m.d-H.i.s").$ext;
		        $dir = 'uploads/banners/'; 
				
				//Fazer upload do arquivo
		        if(move_uploaded_file($_FILES['url']['tmp_name'], $dir.$new_name)) {
					return $result = array("status" => "success",
										   "msg"    => $new_name);
		        } else {
		        	return $result = array("status" => "error",
									   	   "msg"    => "Ocorreram Erros durante o upload da foto");
		        }			        
			} 
		}

		public function removeBannerOld($url)
		{
			$dir = 'uploads/banners/'.$url;

			if (unlink($dir)) {
				return "true";
			} else {
				return "false";
			}

		}

	}